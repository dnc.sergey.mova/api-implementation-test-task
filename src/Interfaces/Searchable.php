<?php


interface Searchable
{
    public function search(array $params);
}