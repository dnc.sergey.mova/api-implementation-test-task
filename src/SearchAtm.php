<?php

require_once "Interfaces/Searchable.php";
require_once 'Exception/WrongParametersException.php';

class SearchAtm implements Searchable
{

    protected const URL = "https://api.privatbank.ua/p24api/infrastructure?json&atm&";

    /**
     *
     * Search function
     *
     * @param  array  $params  Array containing the necessary params.
     *    $params = [
     *      'city'     => (string) City to search
     *      'address' => (string) Address to search
     *      'limit' => (int) Result length
     *      'offset' => (int) Result offset
     *    ]
     *
     * @return array
     * @throws WrongParametersException
     */
    public function search(array $params): array
    {
        if ((!isset($params['city']) || empty($params['city'])) && (!isset($params['address']) || empty($params['address']))) {
            throw new WrongParametersException();
        }

        $data = http_build_query(
            [
                'address' => $params['address'] ?? "",
                'city' => $params['city'] ?? "",
            ]
        );

        $result = json_decode($this->request($data), true);
        if (is_null($result)) {
            return [];
        }

        if (isset($params['limit']) || isset($params['offset'])) {
            $offset = isset($params['offset']) ? intval($params['offset']) : 0;
            $limit = isset($params['limit']) ? intval($params['limit']) : null;

            return array_slice($result['devices'], $offset, $limit);
        }

        return $result['devices'];
    }

    /**
     * @param $data string
     *
     * @return string
     */
    protected function request($data): string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL.$data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

}